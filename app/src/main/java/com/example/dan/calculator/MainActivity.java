package com.example.dan.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Button enter;
    private ImageView backspace;
    private Button zero,one,two,three,four,five,six,seven,eight,nine,decimal;
    private Button openBracket,closeBracket,openParentheses,closeParentheses;
    private Button sin,cos,tan,square,squareroot;
    private Button subtract,add,divide,multiply;
    private TextView display;
    private String[] stack = new String[25];
    private boolean showing = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        display = findViewById(R.id.txtview);
        enter = findViewById(R.id.enterbtn);
        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(display.getText().toString().equals(""));
                else if(errorCheck()) {
                    String d = display.getText().toString();
                    calculate(d);
                    display.setText(stack[0]);
                    showing = true;
                }
                else
                    display.setText("Err");
            }
        });

        backspace = findViewById(R.id.backbtn);
        backspace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String d = display.getText().toString();
                /*if(d.length()>=4 && d.substring(d.length()-2,d.length()).equals("n("))
                    display.setText(d.substring(0,d.length()-4));
                else if(d.substring(d.length()-2,d.length()).equals("√("))
                    display.setText(d.substring(0,d.length()-2));
                else*/
                if(d.length()!=0)
                    display.setText(d.substring(0,d.length()-1));
            }
        });
        backspace.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                display.setText("");
                return false;
            }
        });
        decimal = findViewById(R.id.decimalbtn);
        decimal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isItShowing();
                //String d = display.getText().toString();
                //if(d.substring(d.length()-1).matches(".|X|+|-|"))
                display.setText(display.getText().toString()+".");
            }
        });
        zero = findViewById(R.id.num0);
        zero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isItShowing();
                display.setText(display.getText().toString()+"0");
            }
        });

        one = findViewById(R.id.num1);
        one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isItShowing();
                display.setText(display.getText().toString()+"1");
            }
        });

        two = findViewById(R.id.num2);
        two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isItShowing();
                display.setText(display.getText().toString()+"2");
            }
        });

        three = findViewById(R.id.num3);
        three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isItShowing();
                display.setText(display.getText().toString()+"3");
            }
        });

        four = findViewById(R.id.num4);
        four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isItShowing();
                display.setText(display.getText().toString()+"4");
            }
        });

        five = findViewById(R.id.num5);
        five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isItShowing();
                display.setText(display.getText().toString()+"5");
            }
        });

        six = findViewById(R.id.num6);
        six.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isItShowing();
                display.setText(display.getText().toString()+"6");
            }
        });

        seven = findViewById(R.id.num7);
        seven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isItShowing();
                display.setText(display.getText().toString()+"7");
            }
        });

        eight = findViewById(R.id.num8);
        eight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isItShowing();
                display.setText(display.getText().toString()+"8");
            }
        });

        nine = findViewById(R.id.num9);
        nine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isItShowing();
                display.setText(display.getText().toString()+"9");
            }
        });

        closeBracket = findViewById(R.id.closebracket);
        closeBracket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isItShowing();
                display.setText(display.getText().toString()+"]");
            }
        });

        openBracket = findViewById(R.id.openbracket);
        openBracket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isItShowing();
                display.setText(display.getText().toString()+"[");
            }
        });

        openParentheses = findViewById(R.id.openparenth);
        openParentheses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isItShowing();
                display.setText(display.getText().toString()+"(");
            }
        });

        closeParentheses = findViewById(R.id.closeparenth);
        closeParentheses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isItShowing();
                display.setText(display.getText().toString()+")");
            }
        });

        sin = findViewById(R.id.sinbtn);
        sin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isItShowing();
                display.setText(display.getText().toString()+"sin(");
            }
        });

        cos = findViewById(R.id.cosbtn);
        cos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isItShowing();
                display.setText(display.getText().toString()+"cos(");
            }
        });

        tan = findViewById(R.id.tanbtn);
        tan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isItShowing();
                display.setText(display.getText().toString()+"tan(");
            }
        });

        square = findViewById(R.id.squarebtn);
        square.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isItShowing();
                display.setText(display.getText().toString()+"²");
            }
        });

        squareroot = findViewById(R.id.sqrtbtn);
        squareroot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isItShowing();
                display.setText(display.getText().toString()+"√(");
            }
        });

        subtract = findViewById(R.id.minusbtn);
        subtract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isItShowing();
                display.setText(display.getText().toString()+"-");
            }
        });

        add = findViewById(R.id.addbtn);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isItShowing();
                display.setText(display.getText().toString()+"+");
            }
        });

        multiply = findViewById(R.id.multiplybtn);
        multiply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isItShowing();
                display.setText(display.getText().toString()+"X");
            }
        });

        divide = findViewById(R.id.dividebtn);
        divide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isItShowing();
                display.setText(display.getText().toString()+"/");
            }
        });

    }

    public boolean errorCheck(){
        String str =display.toString();
        int openCnt = 0;
        int closeCnt = 0;
        for(int i = 0;i<str.length();i++){
            if(str.charAt(i)=='(' || str.charAt(i)=='[')
                openCnt++;
            if(str.charAt(i)==')' || str.charAt(i)==']')
                closeCnt++;
        }
        if(openCnt!=closeCnt)
            return false;
        return true;
    }
    public void isItShowing(){
        if(showing) {
            display.setText("");
            showing = false;
        }
    }

    public void calculate(String d) {
        for(int i = 0;i<stack.length;i++){
            stack[i] = "";
        }
        int level = 0;
        Log.d("Good?","Here: "+d);
        for(int i = 0;i<d.length();i++){
            if(d.charAt(i) == '(' || d.charAt(i) == '[')
                level++;
            else if(d.charAt(i) == 't'){
                i+=3;
                level++;
                stack[level] = "tan(";
            }
            else if(d.charAt(i) == 'c'){
                i+=3;
                level++;
                stack[level] = "cos(";
            }
            else if(d.charAt(i) == 's'){
                i+=3;
                level++;
                stack[level] = "sin(";
            }
            else if(d.charAt(i) == '√'){
                i++;
                level++;
                stack[level] = "√(";
            }
            else if(d.charAt(i) == ')' || d.charAt(i) == ']'){
                if(stack[level].contains("tan")) {
                    Log.d("Good?","Here: "+stack[level].substring(4, stack[level].length()));
                    stack[level] = OrderOfOperations(stack[level].substring(4, stack[level].length()));
                    stack[level-1] += Double.toString(Math.tan(Double.parseDouble(stack[level])));
                }
                else if(stack[level].contains("sin")) {
                    stack[level] = OrderOfOperations(stack[level].substring(4, stack[level].length()));
                    stack[level-1] += Double.toString(Math.sin(Double.parseDouble(stack[level])));
                }
                else if(stack[level].contains("cos")){
                    stack[level] = OrderOfOperations(stack[level].substring(4,stack[level].length()));
                    stack[level-1] += Double.toString(Math.cos(Double.parseDouble(stack[level])));
                }
                else if(stack[level].contains("√")){
                    stack[level] = OrderOfOperations(stack[level].substring(2,stack[level].length()));
                    stack[level-1] += Double.toString(Math.sqrt(Double.parseDouble(stack[level])));
                }
                else{
                    stack[level] = OrderOfOperations(stack[level]);
                    stack[level-1] += stack[level];
                }
                stack[level] = "";
                level--;
            }
            else{

                if(stack[level] == null)
                    stack[level] = ""+d.charAt(i);
                else
                    stack[level] += d.charAt(i);
            }
        }
        //Log.d("Good?","Here: "+stack[level]);
        stack[level] = OrderOfOperations(stack[level]);
    }
    public String OrderOfOperations(String temp){
        Log.d("Good?","in Order: "+temp);
        String num = "";
        boolean isitoperator = false;
        double[] nums = new double[26];
        char[] operators = new char[25];
        int opi = 0;
        for(int j = 0; j<temp.length();j++){
            if(temp.charAt(j) == 'X' || temp.charAt(j) == '-' || temp.charAt(j) == '+' || temp.charAt(j) == '/'){
                if(temp.charAt(j) == '-' && !isitoperator) {
                    num += temp.charAt(j);
                    isitoperator = true;
                }
                else{
                    isitoperator = false;
                    if (temp.charAt(j) == 'X')
                        operators[opi] = '*';
                    else
                        operators[opi] = temp.charAt(j);
                    //if(num.length()>0){nums[opi] = Double.parseDouble(num);}

                    nums[opi] = Double.parseDouble(num);
                    num = "";
                    opi++;
                }
            }
            else if(temp.charAt(j) == '²'){
                nums[opi] = Double.parseDouble(num) * Double.parseDouble(num);
                num = "";
            }
            else {
                num += temp.charAt(j);
                isitoperator = true;
            }
        }
        Log.d("Num", "number: "+nums[opi]);
        if(!num.equals(""))
            nums[opi] = Double.parseDouble(num);
        for(int j = 0; j<opi; j++){
            if(operators[j] == '*' )
                nums[j+1] = nums[j] * nums[j+1];
            if(operators[j] == '/')
                nums[j+1] = nums[j] / nums[j+1];
        }
        int type = 0;
        double number = 0;
        for(int j = 0; j<opi; j++){
            if (operators[j] == '+') {
                if(type == 1)
                    nums[j] = number + nums[j];
                if(type == -1)
                    nums[j] = number - nums[j];
                number = nums[j];
                type = 1;
            }
            else if(operators[j] == '-'){
                if(type == 1)
                    nums[j] = number + nums[j];
                if(type == -1)
                    nums[j] = number - nums[j];
                number = nums[j];
                type = -1;
            }
            if(j == opi-1 && type == 1)
                nums[j+1] = nums[j+1] + number;
            if(j == opi-1 && type == -1)
                nums[j+1] =  number - nums[j+1];
        }
        Log.d("Num", "number: "+Double.toString(nums[opi]));
        return Double.toString(nums[opi]);
    }
}
